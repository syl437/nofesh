// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.factories'])

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.scrolling.jsScrolling(true);
})

.run(function($ionicPlatform,$rootScope,DealsFactory) {
    $rootScope.Host = "http://www.tapper.co.il/needy/php";
	$rootScope.Data = null;
	$rootScope.categories = [];
	$rootScope.suppliers = [];
	$rootScope.deals = [];
	$rootScope.USER = [];

	DealsFactory.getData().then(function(data)
	{
		console.log("Data " ,data[1].suppliers)
		$rootScope.suppliers = data[1].suppliers;
		DealsFactory.getDeals();
	});

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'MainCtrl'
        }
      }
    })
	 .state('app.where', {
      url: '/where',
      views: {
        'menuContent': {
          templateUrl: 'templates/where.html',
          controller: 'WhereCtrl'
        }
      }
    })
	
	 .state('app.list', {
      url: '/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/list.html',
          controller: 'ListCtrl'
        }
      }
    })
	 .state('app.deal', {
      url: '/deal/:ItemId',
      views: {
        'menuContent': {
          templateUrl: 'templates/deal.html',
          controller: 'DealCtrl'
        }
      }
    })	
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
});
