angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});  
})

.controller('MainCtrl', function($scope,DealsFactory) 
{
  	 $scope.navTitle='<img class="title-image" src="img/logo.png" />'
	 
	 $scope.NavigateUrl = function(Num)
	 {
		  window.location = "#/app/where"
	 }
})

.controller('WhereCtrl', function($scope,DealsFactory) 
{
 	 $scope.navTitle='<img class="title-image" src="img/logo.png" />'
	 
	 $scope.NavigateUrl = function(Num)
	 {
		window.location = "#/app/list"
     }
})

.controller('ListCtrl', function($scope,$rootScope,DealsFactory) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		$scope.navTitle='<img class="title-image" src="img/logo.png" />'

		$scope.Deals = [];
		$scope.host = $rootScope.Host
		$scope.Deals = $rootScope.deals;
		console.log("Data2 : " , $rootScope.categories )
	 
		$scope.SupplierName = function(SupplierIndex)
		{
			var sName = DealsFactory.getSupplierNameById(SupplierIndex);
			return sName;
		}
		
		$rootScope.$watch('deals', function()
		{
			$scope.Deals = $rootScope.deals;
			console.log("Data2 : " , $rootScope.categories )
		});
	});
})


.controller('DealCtrl', function($scope,DealsFactory,$rootScope,$stateParams,$timeout) 
{
//	$scope.$on('$ionicView.enter', function(e) 
//	{
		$scope.navTitle='<img class="title-image" src="img/logo.png" />'
		$scope.Deal = $rootScope.deals[$stateParams.ItemId];
		$scope.Gallery = [$scope.Deal.image,$scope.Deal.image];
		$scope.host = $rootScope.Host;
		$scope.dealsImageGallery = $scope.Deal.gallery;
		
		console.log($scope.dealsImageGallery)
		
		$scope.SupplierName = function(SupplierIndex)
		{
			var sName = DealsFactory.getSupplierNameById(SupplierIndex);
			return sName;
		}
		
//	}); 
})



.filter('stripslashes', function () {
    return function (value) 
	{
	    return (value + '')
		.replace(/\\(.?)/g, function(s, n1) {
		  switch (n1) {
			case '\\':
			  return '\\';
			case '0':
			  return '\u0000';
			case '':
			  return '';
			default:
			  return n1;
		  }
		});
    };
})


.filter('toTrusted', function ($sce) 
{
    return function (value) {
        return $sce.trustAsHtml(value);
    };
});






